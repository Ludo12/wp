<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ld_site
 */

get_header();
?>

	<!-- #front-page -->
	<div id="primary" class="content-area col-6 mx-auto">
		<main id=" main" class="site-main shadow-lg p-3 mb-5 bg-white rounded">

			<!-- Get the last 2 posts i -->
			<?php if (have_posts()) : ?>
				<?php if (is_home() && !is_front_page()) :
					?>
					<header>
						<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
					</header>
				<?php
				endif; ?>
				<?php $args = array(
					'post_type'   => 'post',
					'post_status' => 'publish',
					'posts_per_page' => 2,
					'orderby' => 'date',
					'order' => 'DESC',
				); ?>
				<?php $my_query = new WP_Query($args); ?>
				<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
					<!-- Do ... -->
					<?php get_template_part('template-parts/content', get_post_format()); ?>
				<?php endwhile; ?>
			<?php else : ?>
				<?php get_template_part('template-parts/content', 'none'); ?>
			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
