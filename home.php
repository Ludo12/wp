<?php

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package ld_site
 */

get_header(); ?>
<!-- home_page -->
<div id="primary" class="content-area col-6 mx-auto">
    <main id="main" class="site-main" role="main">

        <!-- Get the last 2 posts i -->
        <?php if (have_posts()) : ?>
            <?php if (is_home() && !is_front_page()) :
            ?>
                <header>
                    <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                </header>
            <?php
            endif; ?>
            <?php $args = array(
                'post_type'   => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'orderby' => 'date',
                'order' => 'DESC',
            ); ?>
            <?php $my_query = new WP_Query($args); ?>
            <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <!-- Do ... -->
                <?php get_template_part('template-parts/content', get_post_format()); ?>
            <?php endwhile; ?>
        <?php else : ?>
            <?php get_template_part('template-parts/content', 'none'); ?>
        <?php endif; ?>

        <!-- Pour afficher tous les posts -->
        <?php //if (have_posts()) : 
        ?>
        <?php /* Start the Loop */ ?>
        <?php // while (have_posts()) : the_post(); 
        ?>

        <?php // get_template_part('content', get_post_format()); 
        ?>

        <?php // endwhile; 
        ?>
        <?php
        // the_posts_pagination(array(
        // 	'prev_text' => __('&laquo; Previous', 'first'),
        // 	'next_text' => __('Next &raquo;', 'first'),
        // ));
        ?>
        <?php //else : 
        ?>

        <?php // get_template_part('content', 'none'); 
        ?>

        <?php // endif; 
        ?>
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>