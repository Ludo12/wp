<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ld_site
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">
	<?php get_sidebar('footer'); ?>
	<?php if (has_nav_menu('footer')) : ?>
		<nav id="footer-navigation" class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'footer',
				'menu_class'      => 'navbar-nav mr-auto',
				'menu_id'        => 'footer-menu',
				'walker' => new nav_ld_site()
			));
			?>
		</nav><!-- #footer-navigation -->

	<?php endif; ?>
	<div class="site-info">
		<a href="<?php echo esc_url(__('https://wordpress.org/', 'ld_site')); ?>">
			<?php
			/* translators: %s: CMS name, i.e. WordPress. */
			printf(esc_html__('Proudly powered by %s', 'ld_site'), 'WordPress');
			?>
		</a>
		<span class="sep"> | </span>
		<?php
		/* translators: 1: Theme name, 2: Theme author. */
		printf(esc_html__('Theme: %1$s by %2$s.', 'ld_site'), 'ld_site', '<a href="http://ld">ld</a>');
		?>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>

</html>