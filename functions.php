<?php

/**
 * ld_site functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ld_site
 */

if (!function_exists('ld_site_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ld_site_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ld_site, use a find and replace
		 * to change 'ld_site' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('ld_site', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');
		add_image_size('lg-page-thumbnail', 1920, 260, true);

		// This theme uses wp_nav_menu() in two location.
		register_nav_menus(array(
			'primary' => __('Navigation Bar', 'first'),
			'footer' => __('Footer Menu', 'first'),
		));
		// This theme uses wp_nav_menu() in one location.
		// register_nav_menus(array(
		// 	'menu-1' => esc_html__('Primary', 'ld_site'),
		// ));

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// Setup the WordPress core custom header feature.
		add_theme_support('custom-header', apply_filters('ld_site_custom_header_args', array(
			'default-image' => '',
			'width'         => 1920,
			'height'        => 540,
			'flex-height'   => false,
			'header-text'   => false,
		)));

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('ld_site_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		)));

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support('custom-logo');
	}
endif;
add_action('after_setup_theme', 'ld_site_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ld_site_content_width()
{
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters('ld_site_content_width', 640);
}
add_action('after_setup_theme', 'ld_site_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ld_site_widgets_init()
{
	register_sidebar(array(
		'name'          => esc_html__('Sidebar', 'ld_site'),
		'id'            => 'sidebar-1',
		'description'   => esc_html__('Add widgets here.', 'ld_site'),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"
}
add_action('widgets_init', 'ld_site_widgets_init');


/**
 * Enqueue scripts and styles.
 */
function ld_site_scripts()
{
	wp_enqueue_style('style-b','https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css',9);
	wp_enqueue_style('ld_site-style', get_stylesheet_uri());
	wp_enqueue_style('style', get_template_directory_uri() . '/un-style.css');

	wp_enqueue_script('script-b','https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',9);
	wp_enqueue_script('ld_site-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);;
	wp_enqueue_script('ld_site-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'ld_site_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

// Implement the Custom Header feature.
require get_template_directory() . '/inc/nav.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}
