<?php

/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package First
 */

get_header(); ?>

<div id="primary" class="content-area col-6 mx-auto">
	<main id="main" class="site-main" role="main">

		<section class="error-404 not-found">
			<header class="page-header">
				<h1 class="page-title"><?php _e('That page can&rsquo;t be found.', 'first'); ?></h1>

				<img src="https://media.giphy.com/media/A9EcBzd6t8DZe/giphy.gif">
			</header><!-- .page-header -->

			<div class="page-content">
				<p><?php _e('It looks like nothing was found at this location. Maybe try a search?', 'first'); ?></p>

				<?php get_search_form(); ?>
				<!-- Get the last 5 posts i -->
				<?php $args = array(
					'post_type'   => 'post',
					'post_status' => 'publish',
					'posts_per_page' => 5,
					'orderby' => 'post_date',
					'order' => 'DESC',
				); ?>
				<?php $my_query = new WP_Query($args); ?>
				<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
					<!-- Do ... -->
					<h2><?php the_title() ?></h2>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div><!-- .page-content -->

			<footer class="page-footer">
				<?php $args = array(
					'title_li'     => __('Les Pages du site'),
					'depth'        => 1,
				); ?>
				<?php wp_list_pages($args); ?>
			</footer>
		</section><!-- .error-404 -->

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>