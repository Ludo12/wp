<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ld_site
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site col-10 mx-auto">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'ld_site'); ?></a>

		<header id="masthead" class="site-header">
			<div class="site-branding">
				<div class="media">
					<?php
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo_src = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					$logo_srcset = wp_get_attachment_image_srcset( $custom_logo_id  );
					if ( has_custom_logo() ) {
						echo '<img src="' . esc_url( $logo_src[0] ) . '" alt="' . get_bloginfo( 'name' ) . '" srcset="' . $logo_srcset . '">';
					} else {
						echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
					}?>
					<div class="media-body">
						<h1 class="site-title mt-5"><a href=" <?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
					</div>
				</div>
				<?php

				$ld_site_description = get_bloginfo('description', 'display');
				if ($ld_site_description || is_customize_preview()) :
				?>
					<p class="site-description"><?php echo $ld_site_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->
			<?php if (is_home() && get_header_image()) : ?>
                <div id="header-image" class="header-image">
                    <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
                </div><!-- #header-image -->
			<?php elseif (is_page() && has_post_thumbnail()) : ?>
                <div id="header-image" class="header-image-page">
					<?php the_post_thumbnail('lg-page-thumbnail'); ?>
                </div><!-- #header-image page-->
			<?php elseif (is_404()) : ?>
                <div id="header-image" class="header-image-page">
                    <img src="<?php _e(get_template_directory_uri()); ?>/404.png" alt="404">
                </div><!-- #404-image page-->
			<?php endif; ?>
		</header><!-- #masthead -->
        <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


			<?php
			wp_nav_menu(array(
				'theme_location' => 'primary',
				'container_class' => 'collapse navbar-collapse',
				'container_id'    => 'navbarSupportedContent',
				'menu_class'      => 'navbar-nav mr-auto',
				'menu_id'        => 'primary-menu',
				'walker' => new nav_ld_site()
			));
			?>

        </nav>


		<div id="content" class="site-content row pt-5">