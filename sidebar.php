<?php

/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ld_site
 */

if (!is_active_sidebar('sidebar-1')) {
	return;
}
?>

<aside id="secondary" class="widget-area col-3 mx-auto">
	<?php dynamic_sidebar('sidebar-1'); ?>
</aside><!-- #secondary -->