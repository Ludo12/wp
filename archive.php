<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ld_site
 */

get_header();
?>

	<div id="primary" class="content-area col-6 mx-auto">
		<main id="main" class="site-main shadow-lg p-3 mb-5 bg-white rounded">

		<?php

		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

            <!-- Get the last 5 posts i -->
			<?php $args = array(
				'post_type'   => 'post',
				'post_status' => 'publish',
				'posts_per_page' => 10,
				'orderby' => 'post_date',
				'order' => 'DESC',
			); ?>
			<?php $my_query = new WP_Query($args); ?>
			<?php while ($my_query->have_posts()) : $my_query->the_post();
				the_post_thumbnail('medium');
				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;
			$args = wp_parse_args(
				$args,
				array(
					'prev_text'          => 'précedant',
					'next_text'          => 'suivant',
				)
			);
			the_posts_navigation($args);

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
